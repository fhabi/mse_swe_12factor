# SWE 12 Factor App
## FeedbackApi

This project provides a simple ASP.NET Core REST API to create, read, update and delete feedback items.
A feedback item consists of a score from 1-10, an optional comment and a required reference ("What is this feedback about?").

## Setup

Building this project only requires the docker-runtime.
The .NET SDK is provided by using the appropriate docker image.

The scripts directory contains scripts for all relevant tasks:

- build.sh
  - Builds the API project locally resulting in the following images:
    - registry.gitlab.com/fhabi/mse_swe_12factor - API image
    - registry.gitlab.com/fhabi/mse_swe_12factor/db-updater - Image based on .NET SDK for db migration
- push.sh
  - Pushes locally built images to the Gitlab's container registry
    - Requires images to be built first (build.sh)
    - Requires authentication and authorization to push to the project's registry
- start.sh
  - Starts the API and database services
- start-and-migrate.sh
  - Starts the API and database services
  - Starts the db updater service (this runs unapplied migrations on the database)
- stop.sh
  - Stops the API and database services

#### Run locally

To build and run the API locally run (from project root):

```
./scripts/build.sh && ./scripts/start-and-migrate.sh
```

Now the API should be available under http://localhost:1337

## API documentation

The available endpoints and their usage are documented with a Swagger UI document and available under: http://localhost:1337/swagger