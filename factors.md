# The Twelve-Factor App

The factors are considered as described below.

### 1. Codebase

The project uses GIT for version control.
The CI pipeline produces docker images usable in all stages (local developer, ..., production).

### 2. Dependencies

Dependencies are managed using the NuGet package manager for .NET.
During a build, all dependencies needed to run the API are bundled by dotnet publish.

### 3. Config

During development, the service is configured with an appsettings.json file.
At build or at runtime, configuration can be overridden by setting environment variables prefixed with FEEDBACKAPI_.
The process is the same for an arbitrary amount of stages.

### 4. Backing services

The only backing service for the API is postgres database.
The db is connected via a config string that can be overridden by an environment variable (see 3.).

### 5. Build, release, run (not fulfilled)

Since the service is not deployed, this factor is not fulfilled.

The build stage could be supplemented by a release stage:
- Gitlab CI uses docker compose to pull docker images and add stage specific environment variables.

The run stage could simply be Gitlab CI using docker-compose to run the services on the target machine.

### 6. Processes

The service only persists data via the db service. Therefore it can be rerun at any time.

### 7. Port binding

The HTTP service is provided via a port bound from the service container to the host by docker-compose.

NOTE: 
When deploying, an additional reverse proxy container would make sense to manage certificates and route domains/paths to 
the appropriate services.

### 8. Concurrency (not fulfilled)

This factor is out of scope for a service of this complexity.
There are simply no tasks to offload into different processes.

### 9. Disposability (not fulfilled)

The ASP.NET Core service has a startup time of a few seconds.
However, the services do not explicitly handle gracefully shutting down for SIGTERM events / sudden death.

### 10. Dev/prod parity

This project uses the same services in development as it would when deployed (exact same postgres image).
While the developer still has to install .NET SDK locally, quick production-like tests can be done locally via docker-compose.
With Gitlab CI, the developer can also build (and could deploy) images within minutes.

### 11. Logs (partially fulfilled)

The API service writes it's logs to stdout, which can then be accessed via the docker container's logs.

In a deployment scenario, the logs would collected and managed with a tool such as Splunk, so this factor is partially fulfilled.

### 12. Admin processes

The only relevant on-off task for this service is updating the database with migrations.
This factor was achieved by building a secondary image with .NET SDK as the base image.
Using the start-and-migrate script this secondary image then applies migrations and shuts down.

As a result, installing the .NET SDK on the server is not needed.
The code required for this lives in the same repository as the other parts of the service and the dependencies are isolated in the same way.