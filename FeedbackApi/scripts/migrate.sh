#!/bin/bash

dotnet tool install --global dotnet-ef
export PATH="$PATH:/root/.dotnet/tools"
ls -la
ls -la ..
dotnet ef --startup-project ./FeedbackApi.csproj database update