using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FeedbackApi.Models
{
    /// <summary>
    /// Represents feedback given by a person about something.
    /// </summary>
    public class Feedback
    {
        /// <summary>
        /// Unique id.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public string Id { get; set; }
        
        /// <summary>
        /// Point value indicating how good the referenced thing was.
        /// </summary>
        [Range(1, 10)]
        public int Points { get; set; }
        
        /// <summary>
        /// Freetext comment further explaining the given points.
        /// </summary>
        public string Comment { get; set; }
        
        /// <summary>
        /// Short reference indicating what this feedback is about.
        /// </summary>
        [Required]
        public string Reference { get; set; }
    }
}