using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FeedbackApi.Database;
using FeedbackApi.Models;
using Microsoft.AspNetCore.Http;

namespace FeedbackApi.Controllers
{
    /// <summary>
    /// Manages the feedback resource.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class FeedbackController : ControllerBase
    {
        private readonly AppDbContext _context;

        public FeedbackController(AppDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Returns a list of all feedback items.
        /// </summary>
        /// <returns>Array of feedback items.</returns>
        /// <response code="200">Returns list of feedback items.</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Feedback>>> GetFeedbacks()
        {
            return await _context.Feedbacks.ToListAsync();
        }

        /// <summary>
        /// Gets the feedback item identified by the given id.
        /// </summary>
        /// <param name="id">Feedback item id.</param>
        /// <returns>Feedback item.</returns>
        /// <response code="200">Returns found feedback item.</response>
        /// <response code="404">If no feedback items was found with the given id.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Feedback>> GetFeedback(string id)
        {
            var feedback = await _context.Feedbacks.FindAsync(id);

            if (feedback == null)
            {
                return NotFound();
            }

            return feedback;
        }

        /// <summary>
        /// Updates the feedback item identified by the given id.
        /// </summary>
        /// <param name="id">Feedback item id.</param>
        /// <param name="feedback">Feedback item with updated values.</param>
        /// <response code="204">If feedback was updated successfully.</response>
        /// <response code="400">
        ///     If body feedback item id is inconsistent with url segment id or feedback item is invalid.
        /// </response>
        /// <response code="404">If no feedback items was found with the given id.</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutFeedback(string id, Feedback feedback)
        {
            if (id != feedback.Id)
            {
                return BadRequest();
            }

            _context.Entry(feedback).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FeedbackExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a new feedback item.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Feedback
        ///     {
        ///         "points": 6,
        ///         "comment": "Informative presentation.",
        ///         "reference": "DDD presentation"
        ///     }
        /// 
        /// </remarks>
        /// <param name="feedback">Feedback item with initial values.</param>
        /// <returns>Feedback item with added missing values (id).</returns>
        /// <response code="201">Returns created feedback item.</response>
        /// <response code="400">If feedback is invalid.</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Feedback>> PostFeedback(Feedback feedback)
        {
            _context.Feedbacks.Add(feedback);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFeedback", new { id = feedback.Id }, feedback);
        }

        /// <summary>
        /// Deletes the feedback item identified by the given id.
        /// </summary>
        /// <param name="id">Feedback item id.</param>
        /// <response code="204">If feedback item was deleted successfully.</response>
        /// <response code="404">If no feedback items was found with the given id.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteFeedback(string id)
        {
            var feedback = await _context.Feedbacks.FindAsync(id);
            if (feedback == null)
            {
                return NotFound();
            }

            _context.Feedbacks.Remove(feedback);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FeedbackExists(string id)
        {
            return _context.Feedbacks.Any(e => e.Id == id);
        }
    }
}
