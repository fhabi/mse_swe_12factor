using FeedbackApi.Models;
using Microsoft.EntityFrameworkCore;

namespace FeedbackApi.Database
{
    public class AppDbContext : DbContext
    {
        public DbSet<Feedback> Feedbacks { get; set; }
        
        public AppDbContext(DbContextOptions options): base(options) {}
    }
}