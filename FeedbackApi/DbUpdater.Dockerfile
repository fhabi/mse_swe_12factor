FROM mcr.microsoft.com/dotnet/aspnet:5.0 as base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:5.0
COPY . /src
WORKDIR /src
ENTRYPOINT ["scripts/migrate.sh"]